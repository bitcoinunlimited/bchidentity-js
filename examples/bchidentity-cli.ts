import yargs from "yargs";
import * as bchidentity from "../lib/bchidentity";
const bitcoin = require("bitcoinjs-lib");

const argv = yargs
    .command("$0 <bchidentity>", "Authenticate with bchidentity:// URI",
        (args) => {
            args.positional(
                "bchidentity", {
                    demandOption: true,
                    describe: "A bchidentity:// URI",
                    type: "string",
                });
        },
    )
    .option("user", {
        alias: "u",
        default: "alice",
        describe: "Which dummy user (any name is valid)",
    })
    .help()
    .argv;

function fake_rng() {
    const fakernd = Buffer.alloc(32);
    fakernd.fill(argv.user as string);
    return fakernd;
}

async function main() {
    const keypair = bitcoin.ECPair.makeRandom({ rng: fake_rng });
    const { address } = bitcoin.payments.p2pkh({ pubkey: keypair.publicKey });
    const wif = keypair.toWIF();

    console.log("Authenticating to " + argv.bchidentity + " with address "
        + address + " and private key " + wif);

    try {
        const [s, resp] = await bchidentity.identify(argv.bchidentity as string, wif);
        console.log("OK, status code: ", s, " response:" , resp);
    } catch ([s, resp])  {
        console.log("Error, status code: ", s, " reponse: ", resp);
    }
}

main();
